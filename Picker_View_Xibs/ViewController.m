//
//  ViewController.m
//  Picker_View_Xibs
//
//  Created by Bruno Tavares on 23/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) NSArray *pickerData;
@property (nonatomic, strong) UILabel *label;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.pickerData = @[@"Benfica", @"Porto", @"Sporting", @"Paços de Ferreira", @"Boavista"];
    
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 400, 200)];
    pickerView.showsSelectionIndicator = YES;
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    [self.view addSubview:pickerView];
    [self.view addSubview:self.label];
    
    // add constraints to web view
    pickerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.label.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSDictionary *viewsDictionary = @{@"picker":pickerView,
                                      @"label":self.label,
                                      @"superview":self.view};
    
    NSArray *constraint_V_label = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[superview]-(<=1)-[label]"
                                                                            options:NSLayoutFormatAlignAllCenterX
                                                                            metrics:nil
                                                                              views:viewsDictionary];
    
    NSArray *constraint_H_label = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[superview]-(<=1)-[label]"
                                                                            options:NSLayoutFormatAlignAllCenterY
                                                                            metrics:nil
                                                                              views:viewsDictionary];
    
    NSArray *constraint_V_pickerView = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[picker]-100-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:viewsDictionary];
    
    NSArray *constraint_H_pickerView = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[picker]|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:viewsDictionary];
    
    [self.view addConstraints:constraint_H_label];
    [self.view addConstraints:constraint_V_label];
    [self.view addConstraints:constraint_H_pickerView];
    [self.view addConstraints:constraint_V_pickerView];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return self.pickerData[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    self.label.text = self.pickerData[row];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component; {
    
    return self.pickerData.count;
}

@end
